import inquirer from "inquirer"
import copy from './actions/copy.js'
async function  start(name) {

    //提问
    const ans = await inquirer.prompt([
        {
            type: 'list',
            name: 'template',
            message: 'Select a framework',
            choices: [
                {name:'vite-vue'},
                {name:'vite-vue-ts'}
            ]
        }
    ])

    await copy(ans.template,name)
    
}

export default start