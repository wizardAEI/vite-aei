# Vite-vue CLI

### 自用的vite模板，包括router和vuex，提供ts和js两个版本

### 安装方式 

`npm i vite-aei -g`

### 使用方式 

`aei` 查看面板，`aei obj` 创建名为obj的项目

### 特别注意

由于使用了node的cp内置方法，所以需要将node升级至16.7.0及以上才可以使用该CLI

可以使用 node -v 查看node版本

### 其他

有什么需要的插件在gitee上添加评论或联系我的掘金，我会添加上相应的选项

### gitee

https://gitee.com/wizardAEI/vite-aei


### 掘金

https://juejin.cn/user/3457299575741608




