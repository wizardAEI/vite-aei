#!/usr/bin/env node
import {program} from 'commander'
import chalk from 'chalk'
import start from './init.js'

program
    .argument('[option]', 'option, if required', '')
    .action((option) => {
        if(option) {
            start(option)
        }
        else {
            console.log(chalk.rgb(64,158,255)(`创建项目：aei <项目名> ✨✨✨`))
        }
    })

program.parse()
    