import {cp} from 'fs/promises'
import path from 'path'
import chalk from 'chalk'
import { fileURLToPath } from "url";
const __dirname = fileURLToPath(import.meta.url)

export default async (file, objName) => {
    try {
        if(file === 'vite-vue')
        await cp(path.join(__dirname,'../../../vueDemo'), `./${objName}`, {
            recursive:true
        })
        if(file === 'vite-vue-ts')
        await cp(path.join(__dirname,'../../../vueTsDemo'), `./${objName}`, {
            recursive:true
        })
        console.log(chalk.green(`cd ${objName} && npm i 🧩🧩🧩`))
        console.log(chalk.rgb(248,124,141)(`npm run dev 🎉🎉🎉`))
    }
    catch(err) {
        console.log(err)
        console.error('出错啦')
    }
}