import {createRouter, createWebHistory} from 'vue-router'
import home from './views/home.vue'
import other from './views/other.vue'



const routes = [
    {
        name:'home',
        path:'/',
        component:home,
        children:[]
    },
    {
        name:'other',
        path:'/other',
        component:other,
        children:[]
    }
]

const router = createRouter({
    history:createWebHistory(),
    routes
})


router.beforeEach(async (to,from)=>{
    return true
})

export default router